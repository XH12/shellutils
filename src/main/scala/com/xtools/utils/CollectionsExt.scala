package com.xtools.utils

/**im
  * Created by Xeth on 15/7/17.
  */
object CollectionsExt {
  class RichSeq[T](seq:Seq[T]){
    def this(array:Array[T])=this(array.toList)
    def mapIf(condition:(T)=>Boolean, operation:(T)=>T):Seq[T] = seq.map(m=>if(condition(m)) operation(m) else m)
    def mapExcluding(condition:(T)=>Boolean, operation:(T)=>T):Seq[T] = seq.map(m=>if(!condition(m)) operation(m) else m)
  }

  implicit def toRichSeq[T](sequence:Seq[T]) = new RichSeq[T](sequence)
  implicit def toRichArray[T](array:Array[T]) = new RichSeq[T](array)
}
