package com.xtools.utils.InterceptionUtil

import com.xtools.utils.InterceptionUtil.Options.{DetectingOption, StringOption}

/**
  * Created by Xeth on 11/7/17.
  */
object Detector {

  trait InterceptionDetector[X] {
    def isIntercepting: Boolean

    def findLeftOnly: List[X]

    def findRightOnly: List[X]

    def findIntercept: List[X]

    def extract: (List[X], List[X], List[X])
  }

  class DefaultInterceptionDetector[X](left: List[X], right: List[X], dectectingMethod: (List[X], List[X]) => (List[X], List[X], List[X])) extends InterceptionDetector[X] {
    override def isIntercepting: Boolean = extract._2.size!= 0

    override def findLeftOnly: List[X] = extract._1

    override def findRightOnly: List[X] = extract._3

    override def findIntercept: List[X] = extract._2

    override def extract: (List[X], List[X], List[X]) = dectectingMethod(left, right)
  }

  object DefaultInterceptionDetector {
    def apply[X](left: List[X], right: List[X], extractMethod: (List[X], List[X]) => (List[X], List[X], List[X])) = new DefaultInterceptionDetector[X](left, right, extractMethod)

    def get[X](left: List[X], right: List[X], extractMethod: (List[X], List[X]) => (List[X], List[X], List[X])) = DefaultInterceptionDetector[X](left, right, extractMethod)

    def get[_](left: List[_], right: List[_], clazz: Class[_], option: DetectingOption): DefaultInterceptionDetector[_] = get(left,right,clazz,List(option))
    def get[_](left: List[_], right: List[_], clazz: Class[_], options: List[DetectingOption]): DefaultInterceptionDetector[_] = {
      val StringClass = classOf[String]
      clazz match {
        case StringClass =>
          val hasCaseSensitive = options.filter(_.isInstanceOf[StringOption]).find(_ == Options.String.CaseSensitive).nonEmpty
          val hasCaseInsensitive = options.filter(_.isInstanceOf[StringOption]).find(_ == Options.String.CaseInsensitive).nonEmpty
          (hasCaseSensitive, hasCaseInsensitive) match {
            case (false, false) | (true, false) => DefaultInterceptionDetector[String](left.asInstanceOf[List[String]], right.asInstanceOf[List[String]], MethodFactory.DefaultStringDetectingMethod)
            case (false, true) => DefaultInterceptionDetector[String](left.asInstanceOf[List[String]], right.asInstanceOf[List[String]], MethodFactory.DefaultStringDetectingMethod_CI)
            case (true, true) => throw new RuntimeException("Cannot support case sensitive and case insensitive option same time")
          }
        case _ => throw new RuntimeException("Class " + clazz.getName + " not supported")
      }
    }

    def get[_](left: List[_], right: List[_], clazz: Class[_]): DefaultInterceptionDetector[_] = {
      val StringClass = classOf[String]
      clazz match {
        case StringClass => DefaultInterceptionDetector.get(left, right, StringClass, List(Options.String.CaseSensitive))
        case _ => throw new RuntimeException("Class " + clazz.getName + " not supported")
      }
    }
  }
}
