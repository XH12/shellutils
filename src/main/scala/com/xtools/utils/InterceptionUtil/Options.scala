package com.xtools.utils.InterceptionUtil

/**
  * Created by Xeth on 11/7/17.
  */
object Options {
  trait DetectingOption {
    def value: String
  }
  case class StringOption(value: String) extends DetectingOption
  object String {
    val CaseSensitive = StringOption("CS")
    val CaseInsensitive = StringOption("CI")
  }
}
