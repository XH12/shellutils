package com.xtools.utils.InterceptionUtil

/**
  * Created by Xeth on 11/7/17.
  */
object MethodFactory {
  /**
    * This is a standard extract method that will first find unique object and then map them all to check for duplication
    *
    * @tparam X The type of object to be check
    * @return Tuple3 of X object for left only, common shared and right only string list
    */
  def StandDetectingMethod[X] = (left: List[X], right: List[X]) => {
    val result =
      (left ::: right).toSet
        .map((m: X) => Tuple3(m, left.contains(m), right.contains(m)))
        .toList
        .groupBy(m => (m._2, m._3))

    val leftList: List[X] = result.getOrElse((true, false), Nil).map(_._1)
    val rightList: List[X] = result.getOrElse((false, true), Nil).map(_._1)
    val interceptList: List[X] = result.getOrElse((true, true), Nil).map(_._1)
    (leftList, interceptList, rightList)
  }

  /**
    * Compare string list and find of the different between them with casesensitive
    *
    * @return Tuple3 of left only, common shared and right only string list
    */
  def DefaultStringDetectingMethod = (left: List[String], right: List[String]) => StandDetectingMethod[String](left, right)

  /**
    * Compare string list and find of the different between them with case insensitive
    *
    * @return Tuple3 of left only, common shared and right only string list
    */
  def DefaultStringDetectingMethod_CI = (left: List[String], right: List[String]) => StandDetectingMethod[String](left.map(_.toLowerCase), right.map(_.toLowerCase))

}
