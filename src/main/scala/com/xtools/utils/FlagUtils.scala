package com.xtools.utils

/**
  * Created by Xeth on 12/7/17.
  */
object FlagUtils {
  object IntBaseFlag{
    class IntFlag(val flag:Int){
      def isFlagOnFor(valueToCheck:Int):Boolean=(valueToCheck&flag)==flag
      def isFlagOnFor(valueToCheck:IntFlag):Boolean=(valueToCheck.flag&flag)==flag
    }
    object IntFlag{
      def apply(flag:Int): IntFlag = new IntFlag(flag)
      def get(flag:Int):IntFlag = IntFlag(flag)
    }
  }

}
