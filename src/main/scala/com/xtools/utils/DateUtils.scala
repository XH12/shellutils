package com.xtools.utils

import java.text.SimpleDateFormat

/**
  * Created by Xeth on 24/7/17.
  */
object DateUtils {
  object BaseUnit{
    val Year = "yyyy"
    val Month = "MM"
    val Day = "dd"

    val HourAPM="hh"
    val HourDay="HH"
    val Mins = "mm"
    val Second = "ss"
    val MilliSecond = "SSS"
  }
  import BaseUnit._
  private def toDateFormat(df:String):SimpleDateFormat = new SimpleDateFormat(df)
  def YYYYMMDD = toDateFormat(Year+Month+Day)
  def YYYYMMDD_dot = toDateFormat("%s.%s.%s".format(Year,Month,Day))
  def YYYY_MM_DD = toDateFormat("%s_%s_%s".format(Year,Month,Day))

  def HHmmss = toDateFormat("%s:%s:%s".format(HourDay,Mins,Second))
  def DateTime=toDateFormat("%s-%s-%s %s:%s:%s".format(Year,Month,Day,HourDay,Mins,Second))
}
