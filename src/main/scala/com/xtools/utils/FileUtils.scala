package com.xtools.utils


object FileUtils {
  import java.io.File
  class DirectoryBrowsing private[DirectoryBrowsing](private val stream:Stream[File],val root:File,next:Option[DirectoryBrowsing]){

    def asStream:Stream[File]=stream ++ (if(next.isEmpty) Stream.empty else next.get.asStream)

    private[DirectoryBrowsing] def this(root:File,recursive:Boolean)
      = this(if(recursive) DirectoryBrowsing.findAllLayer(root) else DirectoryBrowsing.findRootLayer(root),root,None)

    private[DirectoryBrowsing] def this(main: DirectoryBrowsing,next: DirectoryBrowsing)
    = this(main.stream,main.root,Some(next))

    def excludingRootDirectory = new DirectoryBrowsing(stream.filterNot(f=>f.toString==root.toString),root,next)

    def excludeFile = new DirectoryBrowsing(stream.filterNot(_.isFile),root,next)
    def excludeDirectory = new DirectoryBrowsing(stream.filterNot(_.isDirectory),root,next)
    def excludeHidden=new DirectoryBrowsing(stream.filterNot(_.isHidden),root,next)
    def excludeHiddenDirectory=new DirectoryBrowsing(stream.filterNot(f=>f.isHidden&&f.isDirectory),root,next)
    def excludeHiddenFile=new DirectoryBrowsing(stream.filterNot(f=>f.isHidden&&f.isFile),root,next)

    def findFile = new DirectoryBrowsing(stream.filter(_.isFile),root,next)
    def findDirectory = new DirectoryBrowsing(stream.filter(_.isDirectory),root,next)
    def findHidden = new DirectoryBrowsing(stream.filter(_.isHidden),root,next)
    def findHiddenFile = new DirectoryBrowsing(stream.filter(f=>f.isHidden&&f.isFile),root,next)
    def findHiddenDirectory = new DirectoryBrowsing(stream.filter(f=>f.isHidden&&f.isDirectory),root,next)

    def custFilter(custOperation:(Stream[File])=>Stream[File])=new DirectoryBrowsing(custOperation(stream),root,next)
    def custFilters(custOperations:Array[(Stream[File])=>Stream[File]])={
      new DirectoryBrowsing(custOperations.foldLeft(stream)((first,operation)=>operation(first)),root,next)
    }

    def +(next: DirectoryBrowsing)=new DirectoryBrowsing(this,next)

  }
  object DirectoryBrowsing {
    def findAllLayer(root: File): Stream[File] =
      if (!root.exists) Stream.empty
      else root #:: (
        root.listFiles match {
          case null => Stream.empty
          case files => files.toStream.flatMap(findAllLayer(_))
        })

    def findRootLayer(root:File):Stream[File]=if(!root.exists()) Stream.empty else root #:: (root.listFiles match {
      case null => Stream.empty
      case files=>files.toStream
    })

    def getNonRecursive(root:File)=get(root,false)
    def getRecursive(root:File)=get(root,true)
    def get(root:File,recursive:Boolean)=new DirectoryBrowsing(root,recursive)
  }
}
