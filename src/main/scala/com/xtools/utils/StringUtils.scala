package com.xtools.utils

import java.security.MessageDigest
import javax.xml.bind.annotation.adapters.HexBinaryAdapter

import com.xtools.utils.GeneralTraits.SingleValueOption

/**
  * Created by Xeth on 11/7/17.
  */
object StringUtils {

  object CommonSymbols{
    val comma = ","
    val singleQuote="'"
    val doubleQuote="\""
    val Tab="\t"
    val CR="\r"
    val LF="\n"
    val NewLine:String=CR+LF
    val backSlash = "\\"
    val doubleBackSlash=backSlash+backSlash
    val slash = "/"
  }

  object ASCII_Encoding{
    def toHexString(char:Char):String="%%%s".format(char.toHexString)
    def toHexString(string:String):String=string.map(toHexString).mkString("")
  }

  object BuildDependencies{
    def sbtToMaven(str:String)= {
      val splitedValues = str.replaceAll("%%", "%").split(" % ").map(m => m.replaceAll("\\\"", ""))
      val sbt_package=splitedValues(0)
      val sbt_artifact = splitedValues(1)
      val sbt_version = splitedValues(2)

      """
        |<dependency>
        |	<groupId>%s</groupId>
        |	<artifactId>%s</artifactId>
        |	<version>%s</version>
        |</dependency>
      """.stripMargin.format(sbt_package,sbt_artifact,sbt_version)
    }
  }

  object PasswordHashing {
    object HashOption {
      case class HashOption(value: String) extends SingleValueOption
      val SHA_256 = HashOption("SHA-256")
    }

    def toPasswordHash(password:String):String = toPasswordHash(password,HashOption.SHA_256)
    def toPasswordHash(password: String,hashMethod:HashOption.HashOption): String = {
      assert(password != null)
      val md = MessageDigest.getInstance(hashMethod.value)
      val data = md.digest(password.getBytes)
      new HexBinaryAdapter().marshal(data)
    }
    object splitString{
      def withTab(string:String):Array[String]=string.split(CommonSymbols.Tab)
      def withComma(string:String):Array[String]=string.split(CommonSymbols.comma)
    }
    object splitLines{
      def withTab(string:String):Array[Array[String]]=string.split(CommonSymbols.LF).map(_.split(CommonSymbols.Tab))
      def withComma(string:String):Array[Array[String]]=string.split(CommonSymbols.LF).map(_.split(CommonSymbols.comma))
    }

  }

}
