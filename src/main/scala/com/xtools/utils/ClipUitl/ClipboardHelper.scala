package com.xtools.utils.ClipUitl


import java.awt.Toolkit
import java.awt.datatransfer._

class ClipboardHelper private[ClipboardHelper]() extends ClipboardOwner {
  val clip:Clipboard = Toolkit.getDefaultToolkit.getSystemClipboard
  def write(content: String):Unit = clip.setContents(new StringSelection(content), this)
  def read: String = clip.getContents(this).getTransferData(DataFlavor.stringFlavor).asInstanceOf[String]
  override def lostOwnership(clipboard: Clipboard, contents: Transferable): Unit = {}
}

object ClipboardHelper {
  lazy val self: ClipboardHelper = new ClipboardHelper

  def apply(): ClipboardHelper = self
  def get = ClipboardHelper()
}


